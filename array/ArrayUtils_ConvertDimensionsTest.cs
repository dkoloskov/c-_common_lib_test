﻿using common_lib.array;
using NUnit.Framework;

namespace common_lib_test.array
{
    [TestFixture]
    public class ArrayUtils_ConvertDimensionsTest
    {
        private string[] array1D;
        private string[,] array2D;

        [SetUp]
        public void SetUp()
        {
            //
            array1D = new string[]
            {
                "0:0", "1:0",
                "0:1", "1:1",
                "0:2", "1:2",
                "0:3", "1:3"
            };

            array2D = new string[,]
            {
                {"0:0", "1:0"},
                {"0:1", "1:1"},
                {"0:2", "1:2"},
                {"0:3", "1:3"}
            };
        }

        [Test]
        public void To2dTest()
        {
            string[,] convertedTo2D = ArrayUtils.to2D(array1D, 4, 2);
            Assert.AreEqual(array2D, convertedTo2D);
        }

        [Test]
        public void To1dTest()
        {
            string[] convertedTo1D = ArrayUtils.to1D(array2D);
            Assert.AreEqual(array1D, convertedTo1D);
        }
    }
}