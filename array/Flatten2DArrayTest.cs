﻿using System;
using common_lib.array;
using NUnit.Framework;

namespace common_lib_test.array
{
    [TestFixture]
    public class Flatten2DArrayTest
    {
        private Flatten2DArray<string> array;
        private string[,] expectedArray;

        [SetUp]
        public void SetUp()
        {
            //
            array = new Flatten2DArray<string>()
            {
                {"0:0", "0:1", "0:2", "0:3"},
                {"1:0", "1:1", "1:2", "1:3"}
            };

            //
            expectedArray = new string[,]
            {
                {"0:0", "0:1", "0:2", "0:3"},
                {"1:0", "1:1", "1:2", "1:3"}
            };
        }

        [Test]
        public void IndexerTest()
        {
            //
            array = new Flatten2DArray<string>(2, 4);
            array[0, 0] = "0:0";
            array[0, 1] = "0:1";
            array[0, 2] = "0:2";
            array[0, 3] = "0:3";

            array[1, 0] = "1:0";
            array[1, 1] = "1:1";
            array[1, 2] = "1:2";
            array[1, 3] = "1:3";

            //
            Assert.AreEqual(expectedArray, array);
        }

        [Test]
        public void CollectionInitializerTest()
        {
            Assert.AreEqual(expectedArray, array);
        }

        [Test]
        public void CollectionInitializerTest2()
        {
            Assert.Throws<ArgumentException>(() =>
                {
                    array = new Flatten2DArray<string>()
                    {
                        {"0:0", "0:1", "0:2", "0:3"},
                        {"1:0"}
                    };
                }
            );
        }

        [Test]
        public void ConstructorTest1()
        {
            string[] array1D = new string[]
            {
                "0:0", "0:1", "0:2", "0:3",
                "1:0", "1:1", "1:2", "1:3"
            };
            array = new Flatten2DArray<string>(array1D, 2, 4);

            Assert.AreEqual(expectedArray, array);
        }

        [Test]
        public void ConstructorTest2()
        {
            Assert.Throws<ArgumentException>(() =>
                {
                    string[] array1D = new string[]
                    {
                        "0:0", "0:1", "0:2", "0:3",
                        "1:0", "1:1", "1:2"
                    };
                    array = new Flatten2DArray<string>(array1D, 2, 4);
                }
            );
        }

        [Test]
        public void To2DTest()
        {
            string[,] array2D = array;
            Assert.AreEqual(expectedArray, array2D);
        }
    }
}