﻿using System.Collections.Generic;
using common_lib.array;
using NUnit.Framework;

namespace common_lib_test.array
{
    [TestFixture]
    public class ArrayUtils_ResizeArrayTest
    {
        private string[,] origin2DArray;

        private ResizeArrayArguments[] testCasesArguments = new ResizeArrayArguments[]
        {
            new ResizeArrayArguments(2, 4), // 0 -  Decrease
            new ResizeArrayArguments(2, 4, 1, 1), // 1 - Decrease and take values from center of origin
            new ResizeArrayArguments(6, 8), // 2 - Increase
            new ResizeArrayArguments(6, 8, 0, 0, 1, 1) // 3 - Increase and put values to center of new array
        };

        private List<string[,]> testCasesExpectedResults = new List<string[,]>()
        {
            new string[,]
            {
                {"0:0", "0:1", "0:2", "0:3"},
                {"1:0", "1:1", "1:2", "1:3"}
            }, // 0
            new string[,]
            {
                {"1:1", "1:2", "1:3", "1:4"},
                {"2:1", "2:2", "2:3", "2:4"}
            }, // 1
            new string[,]
            {
                {"0:0", "0:1", "0:2", "0:3", "0:4", "0:5", null, null},
                {"1:0", "1:1", "1:2", "1:3", "1:4", "1:5", null, null},
                {"2:0", "2:1", "2:2", "2:3", "2:4", "2:5", null, null},
                {"3:0", "3:1", "3:2", "3:3", "3:4", "3:5", null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            }, // 2
            new string[,]
            {
                {null, null, null, null, null, null, null, null},
                {null, "0:0", "0:1", "0:2", "0:3", "0:4", "0:5", null},
                {null, "1:0", "1:1", "1:2", "1:3", "1:4", "1:5", null},
                {null, "2:0", "2:1", "2:2", "2:3", "2:4", "2:5", null},
                {null, "3:0", "3:1", "3:2", "3:3", "3:4", "3:5", null},
                {null, null, null, null, null, null, null, null}
            }, // 3
        };

        [SetUp]
        protected void setUp()
        {
            origin2DArray = new string[4, 6]
            {
                {"0:0", "0:1", "0:2", "0:3", "0:4", "0:5"},
                {"1:0", "1:1", "1:2", "1:3", "1:4", "1:5"},
                {"2:0", "2:1", "2:2", "2:3", "2:4", "2:5"},
                {"3:0", "3:1", "3:2", "3:3", "3:4", "3:5"}
            };
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void ResizeArray(int testCaseNum)
        {
            ResizeArrayArguments args = testCasesArguments[testCaseNum];
            string[,] expectedResizedArray = testCasesExpectedResults[testCaseNum];

            //
            string[,] resizedArray = ArrayUtils.ResizeArray(origin2DArray,
                args.NewWidth, args.NewHeight,
                args.OriginalShiftX, args.OriginalShiftY, args.NewShiftX, args.NewShiftY);
            Assert.AreEqual(expectedResizedArray, resizedArray);
        }
    }

    internal class ResizeArrayArguments
    {
        public int NewWidth;
        public int NewHeight;

        public int OriginalShiftX;
        public int OriginalShiftY;

        public int NewShiftX;
        public int NewShiftY;

        public ResizeArrayArguments(int newWidth, int newHeight,
            int originalShiftX = 0, int originalShiftY = 0,
            int newShiftX = 0, int newShiftY = 0)
        {
            this.NewWidth = newWidth;
            this.NewHeight = newHeight;

            this.OriginalShiftX = originalShiftX;
            this.OriginalShiftY = originalShiftY;

            this.NewShiftX = newShiftX;
            this.NewShiftY = newShiftY;
        }
    }
}