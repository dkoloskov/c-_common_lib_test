﻿using common_lib.geom;
using common_lib.more_linq;
using NUnit.Framework;

namespace common_lib_test.more_linq
{
    [TestFixture]
    public class MoreLinq_MinByMaxByTest
    {
        private MinByMaxByArguments[] testCasesArguments = new MinByMaxByArguments[]
        {
            new MinByMaxByArguments
            (
                new Point[]
                {
                    new Point(1, 2), new Point(35, 38), new Point(15, 18),
                    new Point(3, 4), new Point(36, 39), new Point(16, 19),
                    new Point(5, 6), new Point(37, 40), new Point(17, 20)
                },
                37, 1, 40, 2
            ), // 0new MinByMaxByArguments
            new MinByMaxByArguments
            (
                new Point[4]
                {
                    new Point(0, 50), new Point(100, 10), new Point(3, 4), new Point(1, 51)
                },
                100, 0, 51, 4
            ), // 1
        };

        [TestCase(0)]
        [TestCase(1)]
        public void MinByMaxBy(int testCaseNum)
        {
            MinByMaxByArguments args = testCasesArguments[testCaseNum];

            int maxX = args.Array.MaxBy(p => p.X).X;
            Assert.AreEqual(args.ExpectedMaxX, maxX);

            int maxY = args.Array.MaxBy(p => p.Y).Y;
            Assert.AreEqual(args.ExpectedMaxY, maxY);

            int minX = args.Array.MinBy(p => p.X).X;
            Assert.AreEqual(args.ExpectedMinX, minX);

            int minY = args.Array.MinBy(p => p.Y).Y;
            Assert.AreEqual(args.ExpectedMinY, minY);
        }
    }

    internal class MinByMaxByArguments
    {
        public Point[] Array;

        public int ExpectedMaxX;
        public int ExpectedMinX;

        public int ExpectedMaxY;
        public int ExpectedMinY;

        public MinByMaxByArguments(Point[] array, int maxX, int minX, int maxY, int minY)
        {
            this.Array = array;

            this.ExpectedMaxX = maxX;
            this.ExpectedMinX = minX;

            this.ExpectedMaxY = maxY;
            this.ExpectedMinY = minY;
        }
    }
}